package com.example.numberpuzzle

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.recyclerview.widget.RecyclerView
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
class ItemView(context: Context): RecyclerView.Adapter<ItemView.ViewHolder>() {

    var data = emptyList<com.example.numberpuzzle.Item>()
    fun setDataList(data: List<com.example.numberpuzzle.Item>){
        this.data = data
    }
    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
            lateinit var text : TextView
            init {
                text = itemView.findViewById(R.id.item)
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_view, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var data = data[position]
    }

    override fun getItemCount() = data.size

}