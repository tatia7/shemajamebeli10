package com.example.numberpuzzle

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter : ItemView
    private var data = mutableListOf<Item>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = GridLayoutManager(applicationContext, 3)
        adapter = ItemView(applicationContext)
        recyclerView.adapter = adapter

        data.add(Item(1,1))
        data.add(Item(2,2))
        data.add(Item(3,3))
        data.add(Item(4,4))
        data.add(Item(5,5))
        data.add(Item(6,6))
        data.add(Item(7,7))
        data.add(Item(8,8))
        data.add(Item(9,9))
    }
}